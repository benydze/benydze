package com.example.otarybenydze

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {
        buttongenerator.setOnClickListener {
            d("button", "button is activated")
            generatedtext()
        }
    }


    private fun generatedtext(){
        val number: Int = (-100..100).random()
        d("random generated number", "$number")
        if (number % 5 == 0 )
            if (number / 5 > 0 )
                generatedtext.text = "yes"
        else
                generatedtext.text = "no"
    }
}